package com.example.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.fragments.databinding.FragmentRegisterSecondBinding
import com.example.fragments.databinding.FragmentSecondBinding


class RegisterSecond : Fragment() {

    private lateinit var binding: FragmentRegisterSecondBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterSecondBinding.inflate(inflater,container,false)

        return binding.root
    }

}