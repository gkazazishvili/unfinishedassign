package com.example.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.example.fragments.databinding.FragmentFirstBinding
import com.example.fragments.databinding.FragmentSecondBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlin.concurrent.timerTask


class SecondFragment : Fragment() {

    private lateinit var binding: FragmentSecondBinding
    private lateinit var user: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSecondBinding.inflate(inflater,container,false)
        user = FirebaseAuth.getInstance()

        setListener()


        return binding.root
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = user.currentUser
        if(currentUser != null){
            updateUI(currentUser)
        }
    }

    private fun updateUI(currentUser: FirebaseUser) {

    }

    private fun setListener(){
        binding.nextBtn.setOnClickListener {
            findNavController().navigate(R.id.action_secondFragment_to_register_Second)
            registerUser()

        }
    }

    private fun registerUser(){
        val email = binding.etEmailRegistration.text.toString()
        val password = binding.etPasswordRegistration.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            user.createUserWithEmailAndPassword(email, password).addOnCompleteListener{
                task->
                if(task.isSuccessful){
                    val firebaseUser: FirebaseUser = task.result!!.user!!
                }
            }
        }

    }
}


